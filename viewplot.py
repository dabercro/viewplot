from django.http import HttpResponse
from django.shortcuts import render
import os, re

def index(request):
    return HttpResponse()

def rev_name(name):
    return name[::-1]

def view(request):

    share = request.GET.get('share','fuck')
    dirs = request.GET.getlist('dirs',[])
    exprs = request.GET.get('expr','').split()
    width = request.GET.get('width','3')

    regs = r'|'.join(exprs)

    plotdir = '../plots/'
    file_list = []

    allDirs = list(reversed(sorted(os.listdir(plotdir))))

    for directory in dirs:
        if directory not in allDirs:
            continue
        for file_name in os.listdir(plotdir + directory):
            if '.pdf' in file_name:
                file_name = '.'.join(file_name.split('.')[:-1])
                if len(exprs) == 0:
                    file_list.append(os.path.join(directory,file_name))

                elif re.search(regs,file_name):
                    file_list.append(os.path.join(directory,file_name))
                                 
    file_list.sort(key=rev_name)

    passNum = 20

    return render(request,'viewplot/page.html',
                  { 'firstDirs' : allDirs[:passNum],
                    'restDirs' : allDirs[passNum:],
                    'checked' : dirs,
                    'exprs' : exprs,
                    'file_list' : file_list,
                    'plotdir' : plotdir,
                    'passNum' : passNum,
                    'share' : share,
                    'width' : str(100.0/int(width)),
                    'raw_width' : width,
                    })


